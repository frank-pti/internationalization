/*
 * An internationalization library for DotNet
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Internationalization
{
    /// <summary>
    /// Translation string.
    /// </summary>
	public class TranslationString
	{
		private I18n i18n;
		private string domain;
		private string data;
		private object[] arguments;

        /// <summary>
        /// Delegate void for changed event
        /// </summary>
		public delegate void ChangedDelegate(TranslationString translationString);
        /// <summary>
        /// Occurs when changed.
        /// </summary>
		public event ChangedDelegate Changed;

        /// <summary>
        /// Initializes a new instance of the <see cref="Internationalization.TranslationString"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="domain">Domain.</param>
        /// <param name="data">Data.</param>
		public TranslationString(I18n i18n, string domain, string data)
		{
			this.i18n = i18n;
			this.domain = domain;
			this.data = data;
			
			this.i18n.Changed += I18nChanged;
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Internationalization.TranslationString"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="domain">Domain.</param>
        /// <param name="data">Data.</param>
        /// <param name="arguments">Arguments.</param>
		public TranslationString(I18n i18n, string domain, string data, params object[] arguments):
			this(i18n, domain, data)
		{
			Arguments = arguments;
		}

        /// <summary>
        /// Gets or sets the i18n.
        /// </summary>
        /// <value>The i18n.</value>
		public I18n I18n
		{
			get {
				return i18n;
			}
            set {
                if (i18n != value) {
                    i18n = value;
                    I18nChanged(i18n, domain);
                }
            }
		}

        /// <summary>
        /// Gets the domain.
        /// </summary>
        /// <value>The domain.</value>
		public string Domain
		{
			get {
				return domain;
			}
		}

		private void I18nChanged(I18n source, string domain)
		{
			if (domain != this.domain) {
				return;
			}
			NotifyChanged();
		}

        /// <summary>
        /// Gets the translation.
        /// </summary>
        /// <value>The translation.</value>
		public string Tr
		{
			get {
				string raw = i18n.Tr(domain, data);
				if (arguments != null && arguments.Length > 0)
				{
					return String.Format(raw, arguments);
				}
				return raw;
			}
		}

        /// <summary>
        /// Gets a value indicating whether the translation is empty.
        /// </summary>
        /// <value><c>true</c> if this instance is empty; otherwise, <c>false</c>.</value>
		public bool IsEmpty
		{
			get {
				return String.IsNullOrEmpty(Tr);
			}
		}

        /// <summary>
        /// Sets the arguments.
        /// </summary>
        /// <param name="arguments">Arguments.</param>
		public void SetArguments(params object[] arguments)
		{
			Arguments = arguments;
		}

        /// <summary>
        /// Gets or sets the arguments.
        /// </summary>
        /// <value>The arguments.</value>
		public object[] Arguments
		{
			get {
				return arguments;
			}
			set {
				arguments = value;
				NotifyChanged();
			}
		}

		private void NotifyChanged()
		{
			if (Changed != null)
			{
				Changed(this);
			}
		}

        /// <inheritdoc/>
		public override string ToString()
		{
			return Tr;
		}

        /// <summary>
        /// Gets the raw data.
        /// </summary>
        /// <value>The raw data.</value>
        public string RawData {
            get {
                return data;
            }
        }

        /// <summary>
        /// Gets the untranslated string. It is not translated, but formatted using the arguments.
        /// </summary>
        /// <value>The untranslated string.</value>
        public string UntranslatedString {
            get {
                if (arguments != null && arguments.Length > 0)
                {
                     return String.Format(data, arguments);
                }
                return data;
            }
        }
	}
}
