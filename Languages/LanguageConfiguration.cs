/*
 * An internationalization library for DotNet
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Frank.Helpers.Json;
using System.Reflection;

namespace Internationalization.Languages
{
    /// <summary>
    /// Language configuration.
    /// </summary>
	[JsonObject(MemberSerialization.OptIn)]
	public class LanguageConfiguration
	{
		private List<Language> languages;
		
        /// <summary>
        /// Initializes a new instance of the <see cref="Internationalization.Languages.LanguageConfiguration"/> class.
        /// </summary>
		public LanguageConfiguration()
		{
			languages = new List<Language>();
		}

        /// <summary>
        /// List of available languages
        /// </summary>
        /// <value>The languages.</value>
        [JsonProperty("languages")]
		public List<Language> Languages
		{
			get {
				return languages;
			}
			set {
				languages = value;
			}
		}

        /// <summary>
        /// Gets the language identifiers.
        /// </summary>
        /// <value>The language identifiers.</value>
		public List<string> LanguageIds {
			get {
				List<string> list = new List<string>();
				foreach (Language lang in Languages) {
					list.Add(lang.Id);
				}
				return list;
			}
		}

        /// <summary>
        /// Test if a language is contained in the list of available languages.
        /// </summary>
        /// <returns><c>true</c>, if the language with the specified id was containsed, <c>false</c> otherwise.</returns>
        /// <param name="id">language identifier.</param>
		public bool ContainsLanguage(string id)
		{
			foreach(Language language in Languages)
			{
				if (language.Id == id) {
					return true;
				}
			}
			return false;
		}

        /// <summary>
        /// Loads the language configuration from an JSON file resource contained in the calling assembly.
        /// </summary>
        /// <returns>The language configuration.</returns>
        /// <param name="resource">Resource to load from.</param>
        public static LanguageConfiguration LoadFromAssemblyJson(string resource)
        {
            return LoadFromAssemblyJson(Assembly.GetCallingAssembly(), resource);
        }

        /// <summary>
        /// Loads the language configuration from an JSON file resource contained in the specified assembly.
        /// </summary>
        /// <returns>The language configuration.</returns>
        /// <param name="assembly">Assembly with the resource is in.</param>
        /// <param name="resource">Resource to load from.</param>
        public static LanguageConfiguration LoadFromAssemblyJson(Assembly assembly, string resource)
        {
            return Serializer.DeserializeFromAssembly<LanguageConfiguration>(assembly, resource);
        }
	}
}

