/*
 * An internationalization library for DotNet
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Internationalization;
using Newtonsoft.Json;

namespace Internationalization.Languages
{
    /// <summary>
    /// Language.
    /// </summary>
	[JsonObject(MemberSerialization.OptIn)]
	public class Language
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="Internationalization.Languages.Language"/> class.
        /// </summary>
		public Language()
		{
		}
		
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
		[JsonProperty ("id")]
        public string Id {
            get;
            set;
        }
		
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
		[JsonProperty ("name")]
        public string Name {
            get;
            set;
        }
		
        /// <summary>
        /// Gets or sets the English name.
        /// </summary>
        /// <value>The name of the english.</value>
		[JsonProperty ("englishName")]
        public string EnglishName {
            get;
            set;
        }
		
        /// <summary>
        /// Gets the translation of the language name.
        /// </summary>
        /// <returns>The name.</returns>
        /// <param name="i18n">I18n.</param>
		public string TranslatedName(I18n i18n)
		{
            return Translation(i18n).Tr;
		}

        /// <summary>
        /// Gets the translatable string of the language name.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        public TranslationString Translation(I18n i18n)
        {
            return i18n.TrObject(Constants.LanguageDomain, EnglishName);
        }
	}
}

