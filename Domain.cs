/*
 * An internationalization library for DotNet
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
using Newtonsoft.Json;
#endif
using Logging;
using System;

namespace Internationalization
{
    /// <summary>
    /// Internationalization Domain.
    /// </summary>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
#endif
    public class Domain
    {
        private string language;
#if MICRO_FRAMEWORK
        private IDictionary data;
#else
        private Dictionary<string, string> data;
#endif
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="Internationalization.Domain"/> class.
        /// </summary>
        /// <param name="name">Name.</param>
        public Domain(string name)
        {
            this.name = name;
#if MICRO_FRAMEWORK
            data = new Hashtable();
#else
            data = new Dictionary<string, string>();
#endif

        }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>The language.</value>
#if !MICRO_FRAMEWORK
		[JsonProperty("language")]
#endif
        public string Language
        {
            get
            {
                return language;
            }
            set
            {
                language = value;
            }
        }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>The data.</value>
#if MICRO_FRAMEWORK
        public Hashtable Data
#else
        [JsonProperty("data")]
		public Dictionary<string, string> Data
#endif
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                name = value;
            }
        }

        /// <summary>
        /// Gets the string.
        /// </summary>
        /// <returns>The string.</returns>
        /// <param name="input">Input.</param>
        public string GetString(string input)
        {
            if (Contains(input)) {
#if MICRO_FRAMEWORK
                return (string)data[input];
#else
                return data[input];
#endif
            }
            if (input != null && input.Length > 0) {
#if MICRO_FRAMEWORK
                Logger.Log("Untranslated string \"" + input + "\" in domain \"" + name + "\" language " + language);
#else
                Logger.Log("Untranslated string \"{0}\" in domain \"{1}\" language {2}", input, name, language);
#endif
            }
            return input;
        }

        /// <summary>
        /// Tests whether the data contains the specified input.
        /// </summary>
        /// <param name="input">Input.</param>
        public bool Contains(string input)
        {
            bool dataContainsKey = false;
            if (data != null) {
#if MICRO_FRAMEWORK
                dataContainsKey = data.Contains(input);
#else
                dataContainsKey = data.ContainsKey(input);
#endif
                dataContainsKey = dataContainsKey && data[input] != null;
            }
            return input != null && dataContainsKey;
        }

        /// <summary>
        /// Set the specified string to the specified translation.
        /// </summary>
        /// <param name="s">The string.</param>
        /// <param name="translation">The translation.</param>
        public void Set(string s, string translation)
        {
            data[s] = translation;
        }
    }
}

