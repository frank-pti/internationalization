/*
 * An internationalization library for DotNet
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Internationalization
{
    /// <summary>
    /// Attribute for translatable caption.
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class TranslatableCaptionAttribute: TranslatableTextAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Internationalization.TranslatableCaptionAttribute"/> class.
        /// </summary>
        /// <param name="domain">Domain.</param>
        /// <param name="text">Text.</param>
        public TranslatableCaptionAttribute(string domain, string text):
            base(domain, text)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Internationalization.TranslatableCaptionAttribute"/> class.
        /// </summary>
        /// <param name="domain">Domain.</param>
        /// <param name="text">Text.</param>
        /// <param name="shortText">Short text.</param>
        public TranslatableCaptionAttribute(string domain, string text, string shortText):
            base(domain, text, shortText)
        {
        }
    }
}

