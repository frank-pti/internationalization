/*
 * An internationalization library for DotNet
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Internationalization
{
    /// <summary>
    /// Attribute for translatable text.
    /// </summary>
	public class TranslatableTextAttribute: Attribute
	{
		private string domain;
		private string text;
		private string shortText;

        /// <summary>
        /// Initializes a new instance of the <see cref="Internationalization.TranslatableTextAttribute"/> class.
        /// </summary>
        /// <param name="domain">Domain.</param>
        /// <param name="text">Text.</param>
		public TranslatableTextAttribute(string domain, string text):
			this(domain, text, null)
		{
		}
		
        /// <summary>
        /// Initializes a new instance of the <see cref="Internationalization.TranslatableTextAttribute"/> class.
        /// </summary>
        /// <param name="domain">Domain.</param>
        /// <param name="text">Text.</param>
        /// <param name="shortText">Short text.</param>
		public TranslatableTextAttribute(string domain, string text, string shortText)
		{
			this.domain = domain;
			this.text = text;
			this.shortText = shortText;
		}
		
        /// <summary>
        /// Gets the translation.
        /// </summary>
        /// <returns>The translation.</returns>
        /// <param name="i18n">I18n.</param>
		public TranslationString GetTranslation(I18n i18n)
		{
			return i18n.TrObject(domain, text);
		}

        /// <summary>
        /// Gets the short translation if available.
        /// </summary>
        /// <returns>The short translation if available.</returns>
        /// <param name="i18n">I18n.</param>
		public TranslationString GetShortTranslationIfAvailable(I18n i18n)
		{
			if (shortText != null) {
				return i18n.TrObject(domain, shortText);
			}
			return i18n.TrObject(domain, text);
		}

        /// <summary>
        /// Gets a value indicating whether this instance has short text.
        /// </summary>
        /// <value><c>true</c> if this instance has short text; otherwise, <c>false</c>.</value>
		public bool HasShortText {
			get {
				return shortText != null;
			}
		}
	}
}

