/*
 * An internationalization library for DotNet
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

using System.IO;
using System.Reflection;
using Internationalization.Languages;
using System.Globalization;
using Logging;
#if MICRO_FRAMEWORK
using System.Collections;
using IndianaJones.NETMF.Json;
#else
using Frank.Helpers.Json;
using System.Collections.Generic;
using Newtonsoft.Json;
#endif

namespace Internationalization
{
    /// <summary>
    /// Internationalization class.
    /// </summary>
    public class I18n
    {
        /// <summary>
        /// Delegate method for changed event.
        /// </summary>
        public delegate void I18nChangedDelegate(I18n source, string domain);
        /// <summary>
        /// Occurs when changed.
        /// </summary>
        public event I18nChangedDelegate Changed;

        /// <summary>
        /// Delegate void for language changed event.
        /// </summary>
        public delegate void I18nLanguageChangedDelegate(I18n source, string language);
        /// <summary>
        /// Occurs when language changed.
        /// </summary>
        public event I18nLanguageChangedDelegate LanguageChanged;

        private string language;
        private NumberFormatInfo numberFormat;
        private DateTimeFormatInfo dateTimeFormat;

#if MICRO_FRAMEWORK
        private IDictionary domains;
#else
        private Dictionary<string, Domain> domains;
#endif

        /// <summary>
        /// Initializes a new instance of the <see cref="Internationalization.I18n"/> class.
        /// </summary>
        /// <param name="language">Language.</param>
#if MICRO_FRAMEWORK
        public I18n(Assembly assembly, string language)
#else
        public I18n(string language)
#endif
        {
            this.language = language;
#if MICRO_FRAMEWORK
            domains = new Hashtable();
            LoadLanguage(assembly, language);
            LanguageChanged += delegate(I18n i18n, string newLanguage) {
                LoadLanguage(assembly, newLanguage);
            };
#else
            domains = new Dictionary<string, Domain>();
            LoadLanguage(language);
            LanguageChanged += delegate(I18n i18n, string newLanguage) {
                LoadLanguage(newLanguage);
            };
#endif
            CreateFormats();
        }

#if MICRO_FRAMEWORK
        private void LoadLanguage(Assembly assembly, string language)
        {
            string resource = "Internationalization.i18n." + language + ".json";
            LoadFromResource(assembly, Constants.LanguageDomain, resource);
        }
#else
        private void LoadLanguage(string language)
        {
            string resource = String.Format("Internationalization.i18n.{0}.json", language);
            LoadFromResource(Constants.LanguageDomain, resource);
        }

        /// <summary>
        /// Builds from file.
        /// </summary>
        /// <returns>The I18n object.</returns>
        /// <param name="language">Language.</param>
        /// <param name="domain">Domain.</param>
        /// <param name="fileName">File name.</param>
        public static I18n BuildFromFile(string language, string domain, string fileName)
        {
            I18n i18n = new I18n(language);
            i18n.LoadFromFile(domain, fileName);
            return i18n;
        }

        /// <summary>
        /// Builds from resource.
        /// </summary>
        /// <returns>The I18n object.</returns>
        /// <param name="language">Language.</param>
        /// <param name="domain">Domain.</param>
        /// <param name="resource">Resource.</param>
        public static I18n BuildFromResource(string language, string domain, string resource)
        {
            return BuildFromResource(language, domain, resource, Assembly.GetCallingAssembly());
        }
#endif

        /// <summary>
        /// Builds from resource.
        /// </summary>
        /// <returns>The I18n object.</returns>
        /// <param name="language">Language.</param>
        /// <param name="domain">Domain.</param>
        /// <param name="resource">Resource.</param>
        /// <param name="assembly">Assembly.</param>
        public static I18n BuildFromResource(string language, string domain, string resource, Assembly assembly)
        {
#if MICRO_FRAMEWORK
            I18n i18n = new I18n(assembly, language);
#else
            I18n i18n = new I18n(language);
#endif
            i18n.LoadFromResource(assembly, domain, resource);
            return i18n;
        }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>The language.</value>
        public string Language
        {
            set
            {
                if (language != value) {
                    language = value;
                    NotifyLanguageChanged();
                }
            }
            get
            {
                return language;
            }
        }

        /// <summary>
        /// Gets the number format.
        /// </summary>
        /// <value>The number format.</value>
        public NumberFormatInfo NumberFormat
        {
            get
            {
                return numberFormat;
            }
        }

        /// <summary>
        /// Gets the date time format.
        /// </summary>
        /// <value>The date time format.</value>
        public DateTimeFormatInfo DateTimeFormat
        {
            get
            {
                return dateTimeFormat;
            }
        }

        private void NotifyLanguageChanged()
        {
            CreateFormats();
            if (LanguageChanged != null) {
                LanguageChanged(this, language);
            }
        }

        /// <summary>
        /// Loads from file.
        /// </summary>
        /// <param name="domain">Domain.</param>
        /// <param name="fileName">File name.</param>
        public void LoadFromFile(string domain, string fileName)
        {
#if MICRO_FRAMEWORK
            FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            string json = new StreamReader(stream).ReadToEnd();
            Domain d = (Domain)new Serializer().Deserialize(json);
#else
            Domain d = Serializer.DeserializeFile<Domain>(fileName, false);
#endif
            SetDomain(domain, d);
        }

        /// <summary>
        /// Loads from stream.
        /// </summary>
        /// <param name="domain">Domain.</param>
        /// <param name="stream">Stream.</param>
        public void LoadFromStream(string domain, Stream stream)
        {
#if MICRO_FRAMEWORK
            string json = new StreamReader(stream).ReadToEnd();
            Domain d = (Domain)new Serializer().Deserialize(json);
#else
            Domain d = Serializer.DeserializeStream<Domain>(stream);
#endif
            SetDomain(domain, d);
        }

        /// <summary>
        /// Loads from resource.
        /// </summary>
        /// <param name="assembly">Assembly.</param>
        /// <param name="domain">Domain.</param>
        /// <param name="resource">Resource.</param>
        public void LoadFromResource(Assembly assembly, string domain, string resource)
        {
            SetDomain(domain, DeserializeResource<Domain>(assembly, resource));
        }

#if !MICRO_FRAMEWORK
        /// <summary>
        /// Loads from resource.
        /// </summary>
        /// <param name="domain">Domain.</param>
        /// <param name="resource">Resource.</param>
        public void LoadFromResource(string domain, string resource)
        {
            LoadFromResource(Assembly.GetCallingAssembly(), domain, resource);
        }
#endif

        /// <summary>
        /// Saves to file.
        /// </summary>
        /// <param name="domain">Domain.</param>
        /// <param name="fileName">File name.</param>
        public void SaveToFile(string domain, string fileName)
        {
#if MICRO_FRAMEWORK
            bool containsDomain = domains.Contains(domain);
#else
            bool containsDomain = domains.ContainsKey(domain);
#endif
            if (!containsDomain || domains[domain] == null) {
#if MICRO_FRAMEWORK
                string message = "No such domain: \"" + domain + "\"";
#else
                string message = String.Format("No such domain: \"{0}\"", domain);
#endif
                throw new ArgumentException(message);
            }
#if MICRO_FRAMEWORK
            Domain d = (Domain)domains[domain];
#else
            Domain d = domains[domain];
#endif
            SerializeFile(fileName, d);
        }

        private void SerializeFile(string fileName, Domain domain)
        {
            FileStream writeStream = new FileStream(fileName, FileMode.Create);
            TextWriter textWriter = new StreamWriter(writeStream);

#if MICRO_FRAMEWORK
            Serializer serializer = new Serializer();
            serializer.DateFormat = IndianaJones.NETMF.Json.DateTimeFormat.ISO8601;
            string data = serializer.Serialize(domain);

            try {
                textWriter.Write(data);
            } catch (Exception e) {
                throw e;
            } finally {
                textWriter.Close();
                writeStream.Close();
            }
#else
            JsonTextWriter jsonWriter = new JsonTextWriter(textWriter);
            jsonWriter.Formatting = Formatting.Indented;

            try {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(jsonWriter, domain);
            } catch (Exception e) {
                throw e;
            } finally {
                textWriter.Close();
                writeStream.Close();
            }
#endif
        }

        private void CreateFormats()
        {
            CreateNumberFormat();
            CreateDateTimeFormat();
        }

        private void CreateNumberFormat()
        {
#if MICRO_FRAMEWORK
            CultureInfo info = new CultureInfo(language);
#else
            CultureInfo info = CultureInfo.CreateSpecificCulture(language);
#endif
            numberFormat = info.NumberFormat;
        }

        private void CreateDateTimeFormat()
        {
#if MICRO_FRAMEWORK
            CultureInfo info = new CultureInfo(language);
#else
            CultureInfo info = CultureInfo.CreateSpecificCulture(language);
#endif
            dateTimeFormat = info.DateTimeFormat;
        }

        private void SetDomain(string domainName, Domain domain)
        {
            domains[domainName] = domain;
#if MICRO_FRAMEWORK
            ((Domain)domains[domainName]).Name = domainName;
#else
            domains[domainName].Name = domainName;
#endif
            NotifyChanged(domainName);
        }

        /// <summary>
        /// Gets the language of the domain given by the name.
        /// </summary>
        /// <returns>The language.</returns>
        /// <param name="domainName">Domain name.</param>
        public string DomainLanguage(string domainName)
        {
#if MICRO_FRAMEWORK
            return ((Domain)domains[domainName]).Language;
#else
            return domains[domainName].Language;
#endif
        }

#if MICRO_FRAMEWORK
        private static object DeserializeResource(Assembly assembly, string resourceName)
        {
            
            object o = 
        }
#else
        private static T DeserializeResource<T>(Assembly assembly, string resourceName)
        {
            Stream readStream = assembly.GetManifestResourceStream(resourceName);
            if (readStream == null) {
                throw new ArgumentException(String.Format("Resource \"{0}\" not found in assembly {1}", resourceName, assembly.FullName));
            }

            try {
                return Serializer.DeserializeStream<T>(readStream);
            } catch (Exception e) {
                throw e;
            } finally {
                readStream.Close();
            }
        }

        /// <summary>
        /// Translate the specified string s in specified domain using the specified parameters.
        /// </summary>
        /// <param name="domain">Domain.</param>
        /// <param name="s">The string.</param>
        /// <param name="parameters">Parameters.</param>
        public string Tr(string domain, string s, params object[] parameters)
        {
            return String.Format(Tr(domain, s), parameters);
        }
#endif

        /// <summary>
        /// Translate the specified string s in specified domain.
        /// </summary>
        /// <param name="domain">Domain.</param>
        /// <param name="s">The string.</param>
        public string Tr(string domain, string s)
        {
#if MICRO_FRAMEWORK
            if (!domains.Contains(domain) || domains[domain] == null) {
                if (!domains.Contains(domains)) {
                    Logger.Log("Language domain \"" + domain + "\" cannot be found in domains collection. Translation string: \"" + s + "\"");
                } else if (domains[domain] == null) {
                    Logger.Log("Language domain \"" + domain + "\" found but no object connected. Translation string: \"" + s + "\"");
                }
            }
            return ((Domain)domains[domains]).GetString(s);
#else
            if (!domains.ContainsKey(domain) || domains[domain] == null) {
                if (!domains.ContainsKey(domain)) {
                    Logger.Log("Language domain \"{0}\" cannot be found in domains collection. Translation string: \"{1}\"", domain, s);
                } else if (domains[domain] == null) {
                    Logger.Log("Language domain \"{0}\" found, but no object connected. Translation string: \"{1}\"", domain, s);
                }
                return s;
            }
            return domains[domain].GetString(s);
#endif

        }

        /// <summary>
        /// Tests whether the specified string s is contained in the specified domain.
        /// </summary>
        /// <param name="domain">Domain.</param>
        /// <param name="s">The string.</param>
        public bool Contains(string domain, string s)
        {
#if MICRO_FRAMEWORK
            return (domains.Contains(domain) && domains[domain] != null && ((Domain)domains[domain]).Contains(s));
#else
            return (domains.ContainsKey(domain) && domains[domain] != null && domains[domain].Contains(s));
#endif

        }

        /// <summary>
        /// Build a translation object for the specified data in specified domain.
        /// </summary>
        /// <returns>The translation object.</returns>
        /// <param name="domain">Domain.</param>
        /// <param name="data">Data.</param>
        public TranslationString TrObject(string domain, string data)
        {
            return new TranslationString(this, domain, data);
        }

        /// <summary>
        /// Build a translation object for the specified data in specified domain wit the specified arguments.
        /// </summary>
        /// <returns>The translation object.</returns>
        /// <param name="domain">Domain.</param>
        /// <param name="data">Data.</param>
        /// <param name="arguments">Arguments.</param>
        public TranslationString TrObject(string domain, string data, params object[] arguments)
        {
            return new TranslationString(this, domain, data, arguments);
        }

        /// <summary>
        /// Notifies changes.
        /// </summary>
        /// <param name="domain">Domain.</param>
        public void NotifyChanged(string domain)
        {
            if (Changed != null) {
                Changed(this, domain);
            }
        }

        /// <summary>
        /// Set the specified translation for string s in specified domain.
        /// </summary>
        /// <param name="domain">Domain.</param>
        /// <param name="s">The string.</param>
        /// <param name="translation">Translation.</param>
        public void Set(string domain, string s, string translation)
        {
#if MICRO_FRAMEWORK
            if (!domains.Contains(domain) || domains[domain] == null) {
                string message = "No such domain: \"" + domain + "\"";
                throw new Exception(message);
            }
            ((Domain)domains[domain]).Set(s, translation);
#else
            if (!domains.ContainsKey(domain) || domains[domain] == null) {
                throw new Exception(String.Format("No such domain: \"{0}\"", domain));
            }
            domains[domain].Set(s, translation);
#endif
        }

        /// <summary>
        /// Creates the domain.
        /// </summary>
        /// <param name="domain">Domain.</param>
        public void CreateDomain(string domain)
        {
#if MICRO_FRAMEWORK
            if (domains.Contains(domain) && domains[domain] != null) {
                string message = "Domain already exists: \"" + domain + "\"";
                throw new Exception(message);
            }
#else
            if (domains.ContainsKey(domain) && domains[domain] != null) {
                throw new Exception(String.Format("Domain already exists: \"{0}\"", domain));
            }
#endif
            domains[domain] = new Domain(domain);
        }

#if !MICRO_FRAMEWORK
        /// <summary>
        /// Loads the language configuration from resource.
        /// </summary>
        /// <returns>The language configuration.</returns>
        /// <param name="resource">Resource.</param>
        public static LanguageConfiguration LoadLanguageConfiguration(string resource)
        {
            return LoadLanguageConfiguration(Assembly.GetCallingAssembly(), resource);
        }
#endif

        /// <summary>
        /// Loads the language configuration from resource in assembly.
        /// </summary>
        /// <returns>The language configuration.</returns>
        /// <param name="assembly">Assembly.</param>
        /// <param name="resource">Resource.</param>
        public static LanguageConfiguration LoadLanguageConfiguration(Assembly assembly, string resource)
        {
#if MICRO_FRAMEWORK
            string json = 
            return (LanguageConfiguration)new Serializer().Deserialize(json);
#else
            return DeserializeResource<LanguageConfiguration>(assembly, resource);
#endif
        }

        /// <summary>
        /// Initializes the assembly.
        /// </summary>
        /// <param name="assembly">Assembly.</param>
        public void InitializeAssembly(Assembly assembly)
        {
            Attribute[] attributes = Attribute.GetCustomAttributes(assembly, typeof(I18nInitializerAttribute), false);
            if (attributes.Length != 1) {
                throw new ArgumentException(String.Format(
                    "Assembly '{0}' should have exactly one I18nInitializerAttribute, but has {1}",
                    assembly.FullName, attributes.Length));
            }
            (attributes[0] as I18nInitializerAttribute).Initialize(this);
        }
    }
}

