/*
 * An internationalization library for DotNet
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Internationalization
{
    /// <summary>
    /// I18n initializer attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false)]
    public class I18nInitializerAttribute : Attribute
    {
        /// <summary>
        /// Delegate method for initializing the I18n
        /// </summary>
        public delegate void InitializeI18n(I18n i18n);

        /// <summary>
        /// Initializes a new instance of the <see cref="Internationalization.I18nInitializerAttribute"/> class.
        /// </summary>
        /// <param name="initializerType">Initializer type.</param>
        public I18nInitializerAttribute(Type initializerType)
        {
            if (!initializerType.IsSubclassOf(typeof(I18nInitializer))) {
                throw new ArgumentException("I18nInitializerAttribute type must be inherited from I18nInitializer", "initializerType");
            }
            InitializerType = initializerType;
        }

        /// <summary>
        /// Gets the type of the initializer.
        /// </summary>
        /// <value>The type of the initializer.</value>
        public Type InitializerType
        {
            get;
            private set;
        }

        /// <summary>
        /// Initialize the specified i18n.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        public void Initialize(I18n i18n)
        {
            ((I18nInitializer)Activator.CreateInstance(InitializerType)).Initialize(i18n);
        }
    }
}

